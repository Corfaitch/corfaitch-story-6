from django import forms

class FormPesertaBelajar(forms.Form):
    nama_peserta_belajar = forms.CharField(label='Nama peserta belajar', max_length=30)

class FormPesertaMain(forms.Form):
    nama_peserta_main = forms.CharField(label='Nama peserta main', max_length=30)

class FormPesertaTidur(forms.Form):
    nama_peserta_tidur = forms.CharField(label='Nama peserta tidur', max_length=30)
