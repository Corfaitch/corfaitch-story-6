from django.urls import path
from . import views

app_name = 'app6'

urlpatterns = [
    path('', views.index, name = 'index'),
    path('add_peserta_belajar', views.add_peserta_belajar, name='add_peserta_belajar'),
    path('add_peserta_main', views.add_peserta_main, name='add_peserta_main'),
    path('add_peserta_tidur', views.add_peserta_tidur, name='add_peserta_tidur'),
    path('delete_belajar/<int:delete_id>', views.delete_belajar, name='delete_belajar'),
    path('delete_main/<int:delete_id>', views.delete_main, name='delete_main'),
    path('delete_tidur/<int:delete_id>', views.delete_tidur, name='delete_tidur'),
]